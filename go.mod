module gitee.com/wisdom-advisor

go 1.14

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli v1.22.4
	golang.org/x/sys v0.0.0-20200828081204-131dc92a58d5
)
